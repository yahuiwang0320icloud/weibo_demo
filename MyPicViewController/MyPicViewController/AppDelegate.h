//
//  AppDelegate.h
//  MyPicViewController
//
//  Created by hellohome on 2017/7/26.
//  Copyright © 2017年 MeiFuMeiJia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

