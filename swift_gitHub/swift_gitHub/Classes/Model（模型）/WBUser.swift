
import UIKit

/// 微博用户模型
class WBUser: NSObject {

    var id: Int64 = 0

    var title: String?
    
    var imageUrl: String?
    
    var detaile: String?
    
    var name: String?

    override var description: String {
        return yy_modelDescription()
    }
      
}
