
import Foundation


class WBStatusViewModel:CustomStringConvertible{
    

    var status: WBStatus
    
    init(model: WBStatus) {
        self.status = model
    }
    
    var description:String{
        return status.description
    }
}
