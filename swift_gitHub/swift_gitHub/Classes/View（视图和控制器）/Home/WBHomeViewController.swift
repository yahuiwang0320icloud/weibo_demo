//
//  WBHomeViewController.swift
//  swift_gitHub
//
//  Created by hellohome on 2017/7/11.
//  Copyright © 2017年 MeiFuMeiJia. All rights reserved.
//

import UIKit

private let cellId = "cellID"

class WBHomeViewController: WBBaseViewController {
    lazy var statusList = [WBStatusViewModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
    }
    
    override func loadData()  {
        
        WBNetWorkManager.shared.stateList { (list,isSuccess) in
            print(list)
        }
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+1) {
            
            let list:Array =  [["id":111,"text":"这是一个测试 你是我的小苹果 你是我的小苹果 你是我的小苹果 你是我的小苹果 你是我的小苹果 你是我的小苹果 你是我的小苹果 你是我的小苹果 你是我的小苹果  你是我的小苹果 你是我的小苹果 测试结束","user":["id":456,"title":"测试","imageUrl":"tabbar_profile_selected","detaile":"我的小苹果","name":"辉哥开播了"]],["id":222,"text":"这是一个测试 ","user":["id":456,"title":"测试","imageUrl":"tabbar_home_selected","detaile":"我的小苹果","name":"辉哥开播了"]],["id":333,"text":"感觉太爽了","user":["id":456,"title":"测试","imageUrl":"tabbar_message_center_selected","detaile":"我的小苹果","name":"辉哥开播了"]],["id":444,"text":"你说我的rose","user":["id":456,"title":"测试","imageUrl":"icon_small_kangaroo_loading_1","detaile":"我的小苹果","name":"辉哥开播了"]],["id":555,"text":"测试结束","user":["id":456,"title":"测试","imageUrl":"tabbar_compose_idea","detaile":"我的小苹果","name":"辉哥开播了"]]]
            
            var  array = [WBStatusViewModel]()
            
            for dict in list{
                guard let model = WBStatus.yy_model(with: dict) else{
                    continue
                }
                array.append(WBStatusViewModel(model: model))
            }
            
            for _ in 0..<3{
                if self.isPullup {
                    self.statusList += array
                }else{
                    self.statusList += array
                }
            }
    
            self.refreshControl?.endRefreshing()
            self.isPullup = false
            self.tableView?.reloadData()
        }
    }
    
    func showFriends() {
        let vc = WBDemoViewController()
        navigationController?.pushViewController(vc, animated: true)
    }
}

// MARK: 设置数据源方法
extension WBHomeViewController{
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return statusList.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView .dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! WBStatusCell
        if self.statusList.count == 0 {
            return cell
        }
        
        let wb  =  self.statusList[indexPath.row].status
        
        cell.headImageView.image = UIImage.init(named: (wb.user?.imageUrl)!)
        cell.titleLaber.text = wb.user?.title
        cell.detaileLaber.text = wb.user?.detaile
        cell.nickLaber.text = wb.user?.name
        cell.contentLabel.text = wb.text
        return cell
    }
}

extension WBHomeViewController {
    
    override func setUpTableView() {
        super.setUpTableView()
        navItem.leftBarButtonItem = UIBarButtonItem.init(title: "好友", fontSize: 16, target: self, action: #selector(showFriends), isBack: false)
        
        tableView?.register(UINib(nibName: "WBStatusNormalCell", bundle: nil), forCellReuseIdentifier: cellId)
        
        tableView?.rowHeight = UITableViewAutomaticDimension
        tableView?.estimatedRowHeight = 300
        tableView?.separatorStyle = .none
    }
}
