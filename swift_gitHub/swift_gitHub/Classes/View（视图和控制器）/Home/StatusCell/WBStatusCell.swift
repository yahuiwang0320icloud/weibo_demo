//
//  WBStatusCell.swift
//  swift_gitHub
//
//  Created by hellohome on 2017/7/27.
//  Copyright © 2017年 MeiFuMeiJia. All rights reserved.
//

import UIKit

class WBStatusCell: UITableViewCell {

    @IBOutlet weak var headImageView: UIImageView!
    
    @IBOutlet weak var titleLaber: UILabel!
    
    @IBOutlet weak var nickLaber: UILabel!
    
    @IBOutlet weak var detaileLaber: UILabel!
    
    @IBOutlet weak var contentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
