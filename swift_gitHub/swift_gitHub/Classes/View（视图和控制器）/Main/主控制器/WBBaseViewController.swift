//
//  WBBaseViewController.swift
//  swift_gitHub
//
//  Created by hellohome on 2017/7/11.
//  Copyright © 2017年 MeiFuMeiJia. All rights reserved.
//

import UIKit

class WBBaseViewController: UIViewController {
     var userLogOn = true
    
    lazy var navigationBar = UINavigationBar(frame: CGRect(x: 0, y: 0, width: UIScreen.cz_screenWidth(), height: 64))
    
    lazy var navItem = UINavigationItem()
    
    var tableView:UITableView?
    
    var refreshControl: UIRefreshControl? // 刷新视图

    var isPullup = false   //上拉加载更多
    
    /// 访客视图信息字典
    var visitorInfoDictionary: [String: String]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override var title: String?{
        didSet {
           navItem.title = title
        }
    }
}

// MARK: - 设置界面
extension WBBaseViewController {
    
     func setupUI() {
        automaticallyAdjustsScrollViewInsets = false  //设置缩进 如果导航栏隐藏 默认20个像素
        setupNavigationBar()
        userLogOn ? setUpTableView() : setupVisitorView()
  }
    
    
    /// 设置访客视图
     func setupVisitorView() {
        
        let visitorView = WBVisitorView(frame: view.bounds)
        visitorView.visitorInfo = visitorInfoDictionary
        view.insertSubview(visitorView, belowSubview: navigationBar)
        
        visitorView.loginButton.addTarget(self, action: #selector(login), for: .touchUpInside)
        
        visitorView.registerButton.addTarget(self, action: #selector(register), for: .touchUpInside)
        
        navItem.leftBarButtonItem = UIBarButtonItem(title: "注册", style: .plain, target: self, action: #selector(register))
        
        navItem.rightBarButtonItem = UIBarButtonItem(title: "登录", style: .plain, target: self, action: #selector(login))
    }
    
     func setupNavigationBar() {
        view.addSubview(navigationBar)
        navigationBar.items = [navItem]
        
        navigationBar.barTintColor = UIColor.cz_color(withHex: 0xF6F6F6)
        navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.darkGray]
        navigationBar.tintColor = UIColor.orange
    }
    
    func setUpTableView() {
        
        tableView = UITableView.init(frame: view.bounds, style: .plain)
      view.insertSubview(tableView!, belowSubview: navigationBar)
        tableView?.dataSource = self as UITableViewDataSource
        tableView?.delegate = self as UITableViewDelegate
        
        tableView?.contentInset = UIEdgeInsets.init(top: navigationBar.bounds.height, left: 0, bottom: (tabBarController?.tabBar.bounds.height)!, right: 0)
        
        refreshControl = UIRefreshControl()
        tableView?.addSubview(refreshControl!)
        refreshControl?.addTarget(self, action: #selector(loadData), for: .valueChanged)
    
    }
    
    func loadData() {
        refreshControl?.endRefreshing()
    }

}

// MARK: - 访客视图监听方法
extension WBBaseViewController {
    
   func login() {
    NotificationCenter.default.post(name: NSNotification.Name.init(("name")), object: self, userInfo: ["jaj":"sjdfskdf"])
    }
    
    func register() {
        print("用户注册")
    }
}

extension WBBaseViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let row = indexPath.row
        let section = tableView.numberOfSections - 1
        
        if row < 0 || section <  0{
            return
        }
        let count = tableView.numberOfRows(inSection: section)
        if row == (count - 1) && !isPullup{
            isPullup = true
            loadData()
        }
    }
    
}
