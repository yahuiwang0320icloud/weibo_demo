//
//  WBNavigationController.swift
//  swift_gitHub
//
//  Created by hellohome on 2017/7/11.
//  Copyright © 2017年 MeiFuMeiJia. All rights reserved.
//

import UIKit

class WBNavigationController: UINavigationController {
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBar.isHidden = true
    }

    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        
        if viewControllers.count > 0{
            viewController.hidesBottomBarWhenPushed = true
        }
        
        if let vc = viewController as? WBBaseViewController {
            var title = "返回"
            
            if childViewControllers.count == 1 {
                title = childViewControllers.first?.title ?? "返回"
            }
            
             vc.navItem.leftBarButtonItem = UIBarButtonItem(title: title, target: self, action: #selector(popToParent), isBack: true)
        }
        
        super.pushViewController(viewController, animated: true)
    }
    
    func popToParent() {
        popViewController(animated: true)
    }
}
