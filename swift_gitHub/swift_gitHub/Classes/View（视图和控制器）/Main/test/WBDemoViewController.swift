//
//  WBNavigationController.swift
//  swift_gitHub
//
//  Created by hellohome on 2017/7/11.
//  Copyright © 2017年 MeiFuMeiJia. All rights reserved.
//


import UIKit

class WBDemoViewController: WBBaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // 设置标题
        title = "第 \(navigationController?.childViewControllers.count ?? 0) 个"
    }
    
    // MARK: - 监听方法
    /// 继续 PUSH 一个新的控制器
       func showNext() {
        
        let vc = WBDemoViewController()
        
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension WBDemoViewController {
    
    /// 重写父类方法
    override func setUpTableView() {
        super.setUpTableView()
    
        // 设置右侧的控制器
        navItem.rightBarButtonItem = UIBarButtonItem(title: "下一个", target: self, action: #selector(showNext))
    }

}
