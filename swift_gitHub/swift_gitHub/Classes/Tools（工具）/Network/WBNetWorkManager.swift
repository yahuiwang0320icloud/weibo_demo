//
//  WBNetWorkManager.swift
//  swift_gitHub
//
//  Created by hellohome on 2017/7/17.
//  Copyright © 2017年 MeiFuMeiJia. All rights reserved.
//

import UIKit

enum WBHTTPMethod {
    case GET
    case POST
}

class WBNetWorkManager: AFHTTPSessionManager {
    static let shared: WBNetWorkManager = {
        let instance = WBNetWorkManager()
        instance.responseSerializer.acceptableContentTypes?.insert("text/plain")
        return instance
    }()
    
//   封装AFN的GET/POST请求
    func request(method:WBHTTPMethod = .GET, URLString: String,parameters: [String:AnyObject] ,completion:@escaping ((_ json:AnyObject?,_ isSuccessed:Bool)->())){
        
        let success = { (task:URLSessionDataTask,json:AnyObject?)->() in
            completion(json,true)
        }
        
        let failure = { (task:URLSessionDataTask?,error:NSError)->() in
            completion(nil,false)
        }
        
        if method == .GET {
           get(URLString, parameters: parameters, progress: nil, success: success as? (URLSessionDataTask, Any?) -> Void, failure: failure as? (URLSessionDataTask?, Error) -> Void)
        }else{
           post(URLString, parameters: parameters, progress: nil, success: success as? (URLSessionDataTask, Any?) -> Void, failure: failure as? (URLSessionDataTask?, Error) -> Void)
        }
        
    }
}

